﻿using UnityEngine;
using System.Collections;

public class DestroyProjectile : MonoBehaviour {

    private void OnBecameInvisible()
    {
        Destroy(transform.parent.gameObject);
    }
}
