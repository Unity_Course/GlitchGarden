﻿using UnityEngine;
using System.Collections;

public class LoseCollider : MonoBehaviour {

    LevelManager levelManager;

    private void Start()
    {
        levelManager = GameObject.FindObjectOfType<LevelManager>();
        if (!levelManager)
        {
            Debug.LogError("Unable to find LevelManager in scene");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Attacker>())
        {
            Time.timeScale = 0;
            levelManager.LoadLevelAfterDelay("03b_Lose", 5.0f);
        }
    }
}
