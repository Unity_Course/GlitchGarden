﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public float speed;
    public float damage = 10;

	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Attacker>())
        {
            Health targetHealth = collision.gameObject.GetComponent<Health>();
            if (targetHealth)
            {
                targetHealth.DealDamage(damage);
                Destroy(gameObject);
            }
            else
            {
                Debug.LogWarning(collision.name + " has no Health component");
            }
        }
    }
}
