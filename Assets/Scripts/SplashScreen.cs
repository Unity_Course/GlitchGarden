﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {
    public float displayTime = 2.5f;

	// Use this for initialization
	void Start () {
        LevelManager levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        levelManager.LoadNextLevelAfterDelay(displayTime);
    }
}   
