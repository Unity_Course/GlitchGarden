﻿using UnityEngine;
using System.Collections;

public class Attacker : MonoBehaviour {

    [Tooltip ("Average number of seconds between appearances")]
    public float seenEverySeconds;

    private float currentSpeed;
    private GameObject currentTarget;

    protected Animator animator;

    // Use this for initialization
    protected virtual void Start () {
        Rigidbody2D myRigidbody = gameObject.AddComponent<Rigidbody2D>();
        myRigidbody.isKinematic = true;
        gameObject.tag = "Attacker";

        animator = GetComponent<Animator>();
        if (!animator)
        {
            Debug.LogError(name + ": Animator component not found");
        }
    }

    // Update is called once per frame
    void Update () {
        transform.Translate(Vector3.left * currentSpeed * Time.deltaTime);
        if (!currentTarget)
        {
            animator.SetBool("isAttacking", false);
        }
	}

    public void SetSpeed(float speed)
    {
        currentSpeed = speed;
    }

    public void StrikeCurrentTarget(float damage)
    {
        if (currentTarget)
        {
            Health targetHealth = currentTarget.GetComponent<Health>();
            if (targetHealth)
            {
                targetHealth.DealDamage(damage);
            }
            else
            {
                Debug.LogWarning(currentTarget.name + " does not contain a health component");
            }
        }
        else
        {
            Debug.LogWarning(name + ": no current target");
        }
    }

    protected void Attack(GameObject obj)
    {
        currentTarget = obj;
    }
}
