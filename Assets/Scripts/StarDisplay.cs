﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof(Text))]
public class StarDisplay : MonoBehaviour {

    public enum Status { SUCCESS, FAILURE };

    private Text text;
    private int starCount = 100;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        if (!text)
        {
            Debug.LogWarning("StarDisplay: Unable to find text component");
        }

        UpdateDisplay();
	}
	
    public void AddStars(int amount)
    {
        starCount += amount;
        UpdateDisplay();
    }

    public Status UseStars(int amount)
    {
        if (starCount >= amount)
        {
            starCount -= amount;
            UpdateDisplay();
            return Status.SUCCESS;
        }

        return Status.FAILURE;
    }

    void UpdateDisplay()
    {
        text.text = starCount.ToString();
    }
}
