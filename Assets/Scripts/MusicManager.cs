﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{

    public AudioClip[] levelMusicChangeArray;
    public AudioClip victorySound;

    private AudioSource audioSource;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        SetVolume(PlayerPrefsManager.GetMasterVolume());
    }

    private void OnLevelWasLoaded(int level)
    {
        AudioClip music = levelMusicChangeArray[level];
        if (music && music != audioSource.clip)
        {
            audioSource.clip = music;
            audioSource.loop = true;
            audioSource.Play();
        }
    }

    public void SetVolume(float volume)
    {
        audioSource.volume = volume;
    }

    public void PlayVictorySound()
    {
        audioSource.clip = victorySound;
        audioSource.loop = false;
        audioSource.Play();
    }
}
