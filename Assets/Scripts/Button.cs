﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

    public GameObject defender;
    static public GameObject selectedDefender;

    private Button[] buttonArray;

	// Use this for initialization
	void Start () {
        buttonArray = transform.parent.GetComponentsInChildren<Button>();

        // Select first button in array
        if (gameObject.name == buttonArray[0].gameObject.name)
        {
            SetColor(Color.white);
            selectedDefender = defender;
        }
        else
        {
            SetColor(Color.black);
        }
	}

    public void Select()
    {
        foreach (Button button in buttonArray)
        {
            button.SetColor(Color.black);
        }
        SetColor(Color.white);
        selectedDefender = defender;
    }

    public void SetColor(Color color)
    {
        GetComponent<SpriteRenderer>().color = color;
    }

    private void OnMouseDown()
    {
        Select();
    }
}
