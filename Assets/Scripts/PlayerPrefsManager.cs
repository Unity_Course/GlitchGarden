﻿using UnityEngine;
using System.Collections;

public class PlayerPrefsManager : MonoBehaviour {

    public const float DEFAULT_MASTER_VOLUME = 0.75f;
    public const float DEFAULT_DIFFICULTY = 2.0f;

    const string MASTER_VOLUME_KEY = "master_volume";
    const string DIFFICULTY_KEY = "difficulty";
    const string LEVEL_KEY = "level_unlocked_";

    public static void SetMasterVolume(float volume)
    {
        if (volume >= 0.0f && volume <= 1.0f)
        {
            PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, volume);
        }
        else
        {
            Debug.LogError("Master volume out of range");
        }
    }

    public static float GetMasterVolume()
    {
        if (PlayerPrefs.HasKey(MASTER_VOLUME_KEY))
        {
            return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
        }
        return DEFAULT_MASTER_VOLUME;
    }

    public static void UnlockLevel(int level)
    {
        if (level <= Application.levelCount - 1 && level >= 0)
        {
            PlayerPrefs.SetInt(LEVEL_KEY + level.ToString(), 1);
        }
        else
        {
            Debug.LogError("Trying to unlock an invalid level");
        }
    }

    public static bool IsLevelUnlocked(int level)
    {
        if (level <= Application.levelCount - 1 && level >= 0)
        {
            return PlayerPrefs.GetInt(LEVEL_KEY + level.ToString()) == 1;
        }
        else
        {
            Debug.LogError("Trying to query an invalid level");
        }
        return false;
    }

    public static void SetDifficulty(float difficulty)
    {
        if (difficulty >= 1.0f && difficulty <= 3.0f)
        {
            PlayerPrefs.SetFloat(DIFFICULTY_KEY, difficulty);
        }
        else
        {
            Debug.LogError("Difficulty out of range");
        }
    }

    public static float GetDifficulty()
    {
        if (PlayerPrefs.HasKey(DIFFICULTY_KEY))
        {
            return PlayerPrefs.GetFloat(DIFFICULTY_KEY);
        }
        return DEFAULT_DIFFICULTY;
    }
}
