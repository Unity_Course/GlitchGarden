﻿using UnityEngine;
using System.Collections;

public class DefenderSpawner : MonoBehaviour {

    GameObject defenderParent;

    private StarDisplay starDisplay;

    private void Start()
    {
        defenderParent = GameObject.Find("Defenders");
        if (!defenderParent)
        {
            defenderParent = new GameObject("Defenders");
        }

        starDisplay = GameObject.FindObjectOfType<StarDisplay>();
        if (!starDisplay)
        {
            Debug.LogError("Cannot find StarDisplay");
        }
    }

    private void OnMouseDown()
    {
        if (starDisplay.UseStars(Button.selectedDefender.GetComponent<Defender>().starCost) == StarDisplay.Status.SUCCESS)
        {
            GameObject defender = Instantiate(Button.selectedDefender, SnapToGrid(CalculateWorldPointOfMouseClick()), Quaternion.identity) as GameObject;
            defender.transform.parent = defenderParent.transform;
        }
        else
        {
            Debug.Log("Insufficient Stars");
        }
    }

    Vector2 SnapToGrid(Vector2 rawWorldPos)
    {
        return new Vector2(Mathf.Round(rawWorldPos.x), Mathf.Round(rawWorldPos.y));
    }

    Vector2 CalculateWorldPointOfMouseClick()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}
