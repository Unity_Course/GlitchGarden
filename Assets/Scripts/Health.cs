﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

    public float health = 100.0f;

    public float DealDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            DestroyObject();
        }
        return health;
    }

    public void DestroyObject()
    {
        Destroy(gameObject);
    }
}
