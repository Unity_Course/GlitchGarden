﻿using UnityEngine;
using System.Collections;

public class Lizard : Attacker {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject obj = collision.gameObject;

        if (!obj.GetComponent<Defender>())
        {
            return;
        }

        Attack(obj);
        animator.SetBool("isAttacking", true);
    }
}
