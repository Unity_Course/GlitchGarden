﻿using UnityEngine;
using System.Collections;

public class Fox : Attacker {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject obj = collision.gameObject;

        if (!obj.GetComponent<Defender>())
        {
            return;
        }

        if (obj.GetComponent<Stone>())
        {
            animator.SetTrigger("jumpTrigger");
        }
        else
        {
            Attack(obj);
            animator.SetBool("isAttacking", true);
        }
    }
}
