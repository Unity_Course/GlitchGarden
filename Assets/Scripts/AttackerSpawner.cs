﻿using UnityEngine;
using System.Collections;

public class AttackerSpawner : MonoBehaviour {

    public GameObject[] attackerPrefabArray;

	// Update is called once per frame
	void Update () {
        foreach (GameObject thisAttacker in attackerPrefabArray)
        {
            if (IsTimeToSpawn(thisAttacker))
            {
                Spawn(thisAttacker);
            }
        }
    }

    private bool IsTimeToSpawn(GameObject attackerToSpawn)
    {
        Attacker attacker = attackerToSpawn.GetComponent<Attacker>();
        if (!attacker)
        {
            Debug.LogWarning("Attacker component not found");
            return false;
        }

        if (Time.deltaTime > attacker.seenEverySeconds)
        {
            Debug.LogWarning("Spawn rate capped by frame rate");
        }

        float spawnsPerSecond = 1 / attacker.seenEverySeconds;
        float threshold = spawnsPerSecond * Time.deltaTime / 5; // Account for all 5 lanes

        return (Random.value < threshold);
    }

    private void Spawn(GameObject toSpawn)
    {
        GameObject obj = Instantiate(toSpawn, transform.position, Quaternion.identity) as GameObject;
        obj.transform.parent = transform;
    }
}
