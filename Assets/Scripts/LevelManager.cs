﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

    private bool isTimerActive = false;
    private float timerStart;
    private float timerDelay;
    private string levelToLoad = "";

    private void Update()
    {
        if (isTimerActive)
        {
            if (Time.realtimeSinceStartup >= timerStart + timerDelay)
            {
                if (levelToLoad == "")
                {
                    LoadNextLevel();
                }
                else
                {
                    LoadLevel(levelToLoad);
                }
                isTimerActive = false;
                levelToLoad = "";
            }
        }
    }

    public void LoadLevel(string name)
    {
        Application.LoadLevel(name);
    }

    public void LoadNextLevel()
    {
        Application.LoadLevel(Application.loadedLevel + 1);
        // Reset time scale in case it was altered previously
        Time.timeScale = 1;
    }

    public void LoadNextLevelAfterDelay(float delay)
    {
        isTimerActive = true;
        timerStart = Time.realtimeSinceStartup;
        timerDelay = delay;
    }

    public void QuitRequest()
    {
        Application.Quit();
    }

    public void LoadLevelAfterDelay(string level, float delay)
    {
        isTimerActive = true;
        timerStart = Time.realtimeSinceStartup;
        timerDelay = delay;
        levelToLoad = level;
    }
}
