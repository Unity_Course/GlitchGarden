﻿using UnityEngine;
using System.Collections;

public class Shooter : Defender {

    public GameObject projectile;
    public float maxFiringRate = 2.0f;

    private GameObject projectileParent;
    private float lastFired = 0;
    private bool canFire = true;
    private Animator animator;
    private AttackerSpawner myLaneSpawner;

    private void Start()
    {
        projectileParent = GameObject.Find("Projectiles");

        if (!projectileParent)
        {
            projectileParent = new GameObject("Projectiles");
        }

        animator = gameObject.GetComponent<Animator>();
        if (!animator)
        {
            Debug.LogWarning(name + ": Animator not found");
        }

        SetMyLaneSpawner();
    }

    private void Update()
    {
        if (!canFire)
        {
            lastFired += Time.deltaTime;
            if (lastFired >= maxFiringRate)
            {
                lastFired = 0;
                canFire = true;
            }
        }

        if (IsAttackerAheadInLane())
        {
            animator.SetBool("isAttacking", true);
        }
        else
        {
            animator.SetBool("isAttacking", false);
        }
    }

    bool IsAttackerAheadInLane()
    {
        if (myLaneSpawner.transform.childCount > 0)
        {
            foreach (Transform attackerTransform in myLaneSpawner.transform)
            {
                if (attackerTransform.position.x > transform.position.x)
                    return true;
            }
        }
        return false;
    }

    void SetMyLaneSpawner()
    {
        AttackerSpawner[] spawnerArray = GameObject.FindObjectsOfType<AttackerSpawner>();
        foreach (AttackerSpawner spawner in spawnerArray)
        {
            if (spawner.transform.position.y == transform.position.y)
            {
                myLaneSpawner = spawner;
                return;
            }
        }

        Debug.LogWarning("No spawner found in lane");
    }

    private void FireGun()
    {
        Transform gunTransform = transform.Find("Gun");
        if (gunTransform)
        {
            if (canFire)
            {
                GameObject newProjectile = Instantiate(projectile, gunTransform.position, Quaternion.identity) as GameObject;
                newProjectile.transform.SetParent(projectileParent.transform);
                canFire = false;
            }
        }
        else
        {
            Debug.LogWarning(name + ": no Gun child found");
        }
    }
}
