﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof(Image))]
public class PanelFade : MonoBehaviour {

    public float fadeDuration = 2.0f;

    private Color startColor = Color.black;
    private bool isFaded = false;
    private float fadeTime = 0.0f;
    Image image;

	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
        ResetFade();
	}

    private void Update()
    {
        if (!isFaded)
        {
            fadeTime += Time.deltaTime;
            float fade = 1 - (fadeTime / fadeDuration);
            fade = Mathf.Max(fade, 0.01f);
            image.canvasRenderer.SetAlpha(fade);
            if (fade <= 0.01f)
            {
                isFaded = true;
            }
        }
    }

    private void ResetFade()
    {
        image.color = startColor;
        isFaded = false;
        fadeTime = 0.0f;
    }
}
