﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof(Slider))]
public class GameTimer : MonoBehaviour {

    [Tooltip ("Time in seconds needed to survive level")]
    public float levelTime = 120;
    public GameObject levelCompleteText;

    private Slider slider;
    private LevelManager levelManager;
    private MusicManager musicManager;
    private bool levelComplete = false;

	// Use this for initialization
	void Start () {
        slider = GetComponent<Slider>();
        if (!slider)
        {
            Debug.LogWarning("No slider component found");
        }

        levelManager = GameObject.FindObjectOfType<LevelManager>();
        if (!levelManager)
        {
            Debug.LogError("No LevelManager found");
        }

        musicManager = GameObject.FindObjectOfType<MusicManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!levelComplete)
        {
            float percentDone = Time.timeSinceLevelLoad / levelTime;
            slider.value = percentDone;
            if (percentDone >= 1.0f)
            {
                CompleteLevel();
            }
        }
	}

    void CompleteLevel()
    {
        levelComplete = true;
        Time.timeScale = 0;
        levelManager.LoadNextLevelAfterDelay(5.0f);
        if (musicManager)
        {
            musicManager.PlayVictorySound();
        }
        if (levelCompleteText)
        {
            levelCompleteText.SetActive(true);
        }
    }
}
